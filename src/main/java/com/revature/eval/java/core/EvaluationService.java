package com.revature.eval.java.core;



import java.time.temporal.Temporal;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import static java.lang.Character.*;

public class EvaluationService {

	/**
	 * 1. Without using the StringBuilder or StringBuffer class, write a method that
	 * reverses a String. Example: reverse("example"); -> "elpmaxe"
	 * 
	 * @param
	 * @return
	 *
	 *
	 * Completed 2-22-22 4 PM Arizona Time
	 */
	public static String reverse(String message) {
		String backwards = "";
		char[] forwards = message.toCharArray();
		for (int i = forwards.length - 1;i > -1; i-- ){
			backwards = backwards + forwards[i];}

		return backwards;
	}

	/**
	 * 2. Convert a phrase to its acronym. Techies love their TLA (Three Letter
	 * Acronyms)! Help generate some jargon by writing a program that converts a
	 * long name like Portable Network Graphics to its acronym (PNG).
	 * 
	 * @param phrase
	 * @return
	 *
	 * Completed 2-22-22 5:30PM Arizona Time
	 */
	public String acronym(String phrase) {
		char[] grid = phrase.toCharArray();
		String acronym = "";
		for (int i = 0; i < grid.length -1; i++)
		{
		if (i==0) {acronym = acronym + grid[i];}

		else if (grid[i] == ' ') {acronym = acronym + grid[i+1];}

		else if (grid[i] == '_'|| grid[i] == '-'){acronym = acronym + grid[i+1];}


		}

		return acronym.toUpperCase();
	}

	/**
	 * 3. Determine if a triangle is equilateral, isosceles, or scalene. An
	 * equilateral triangle has all three sides the same length. An isosceles
	 * triangle has at least two sides the same length. (It is sometimes specified
	 * as having exactly two sides the same length, but for the purposes of this
	 * exercise we'll say at least two.) A scalene triangle has all sides of
	 * different lengths.
	 *
	 *
	 * Completed 2-22-22 5:35PM Arizona Time
	 */
	static class Triangle {
		private double sideOne;
		private double sideTwo;
		private double sideThree;

		public Triangle() {
			super();
		}

		public Triangle(double sideOne, double sideTwo, double sideThree) {
			this();
			this.sideOne = sideOne;
			this.sideTwo = sideTwo;
			this.sideThree = sideThree;
		}

		public double getSideOne() {
			return sideOne;
		}

		public void setSideOne(double sideOne) {
			this.sideOne = sideOne;
		}

		public double getSideTwo() {
			return sideTwo;
		}

		public void setSideTwo(double sideTwo) {
			this.sideTwo = sideTwo;
		}

		public double getSideThree() {
			return sideThree;
		}

		public void setSideThree(double sideThree) {
			this.sideThree = sideThree;
		}

		public boolean isEquilateral() {
			if((getSideOne() == getSideTwo()) && (getSideTwo() == getSideThree()) )
			{return true;}
			else{return false;}
		}

		public boolean isIsosceles() {
			if( ( getSideOne() == getSideTwo() ) || getSideOne() == getSideThree()
			     || getSideTwo() == getSideThree()  )
			{return true;}
			else {return false;}
		}

		public boolean isScalene() {
			if( ( getSideOne() != getSideTwo() ) && getSideOne() != getSideThree()
					& getSideTwo() != getSideThree()  )
			{return true;}
			else {return false;}
		}

	}

	/**
	 * 4. Given a word, compute the scrabble score for that word.
	 * 
	 * --Letter Values-- Letter Value A, E, I, O, U, L, N, R, S, T = 1; D, G = 2; B,
	 * C, M, P = 3; F, H, V, W, Y = 4; K = 5; J, X = 8; Q, Z = 10; Examples
	 * "cabbage" should be scored as worth 14 points:
	 * 
	 * 3 points for C, 1 point for A, twice 3 points for B, twice 2 points for G, 1
	 * point for E And to total:
	 * 
	 * 3 + 2*1 + 2*3 + 2 + 1 = 3 + 2 + 6 + 3 = 5 + 9 = 14
	 * 
	 * @param string
	 * @return
	 *
	 * Completed 2-22-22 6:35 PM Arizona Time
	 */
	public int getScrabbleScore(String word) {
		int score = 0;
		word = word.toUpperCase();
		char[] letters = word.toCharArray();

		HashMap<Character,Integer> values = new HashMap<>();
		values.put('A',1);
		values.put('B',3);
		values.put('C',3);
		values.put('D',2);
		values.put('E',1);
		values.put('F',4);
		values.put('G',2);
		values.put('H',4);
		values.put('I',1);
		values.put('J',8);
		values.put('K',5);
		values.put('L',1);
		values.put('M',3);
		values.put('N',1);
		values.put('O',1);
		values.put('P',3);
		values.put('Q',10);
		values.put('R',1);
		values.put('S',1);
		values.put('T',1);
		values.put('U',1);
		values.put('V',4);
		values.put('W',4);
		values.put('X',8);
		values.put('Y',4);
		values.put('Z',10);

		for(int i = 0; i < letters.length; i++)
		{score = score + (values.get(letters[i]));}

		return score;
	}

	/**
	 * 5. Clean up user-entered phone numbers so that they can be sent SMS messages.
	 * 
	 * Completed 2-23-22 4:30 PM Arizona Time
	 */
	public String cleanPhoneNumber(String number) {
		String localNumber = "";
		char[] dubstep = number.toCharArray();


		for (int i = 0; i < dubstep.length; i++) {
			if (
					(dubstep[i] == '0' || dubstep[i] == '1' || dubstep[i] == '2' ||
					dubstep[i] == '3' || dubstep[i] == '4' || dubstep[i] == '5' ||
					dubstep[i] == '6' || dubstep[i] == '7' || dubstep[i] == '8' ||
					dubstep[i] == '9')

			)



			{localNumber = localNumber + dubstep[i];}


		}
		if(localNumber.length() > 10 ) {throw new IllegalArgumentException();}
		if(localNumber.length() < 10 ) {throw new IllegalArgumentException();}

		else {return localNumber;}
	}





	/**
	 * 6. Given a phrase, count the occurrences of each word in that phrase.
	 * 
	 * For example for the input "olly olly in come free" olly: 2 in: 1 come: 1
	 * free: 1
	 * 
	 * @param string
	 * @return
	 */
	public Map<String, Integer> wordCount(String s) {
		s = s.toLowerCase() + " ";
		String WordOnDeck = "";
		int i = 0;

		HashMap<String, Integer> counter = new HashMap<>();

		for (i = 0; i < s.length();i++)

			if (	s.charAt(i) == 'a' ||
					s.charAt(i) == 'b' ||
					s.charAt(i) == 'c' ||
					s.charAt(i) == 'd' ||
					s.charAt(i) == 'e' ||
					s.charAt(i) == 'f' ||
					s.charAt(i) == 'g' ||
					s.charAt(i) == 'h' ||
					s.charAt(i) == 'i' ||
					s.charAt(i) == 'j' ||
					s.charAt(i) == 'k' ||
					s.charAt(i) == 'l' ||
					s.charAt(i) == 'm' ||
					s.charAt(i) == 'n' ||
					s.charAt(i) == 'o' ||
					s.charAt(i) == 'p' ||
					s.charAt(i) == 'q' ||
					s.charAt(i) == 'r' ||
					s.charAt(i) == 's' ||
					s.charAt(i) == 't' ||
					s.charAt(i) == 'u' ||
					s.charAt(i) == 'v' ||
					s.charAt(i) == 'w' ||
					s.charAt(i) == 'x' ||
					s.charAt(i) == 'y' ||
					s.charAt(i) == 'z' ||
					s.charAt(i) == '-'
					)
			{WordOnDeck = WordOnDeck + s.charAt(i);
				continue;						}

			else if (s.charAt(i) == ' ' || s.charAt(i) == ',') {

				if (WordOnDeck == "") {continue;}

				 else {


					if (counter.containsKey(WordOnDeck)) {
						int oldValue = counter.get(WordOnDeck);
						counter.replace(WordOnDeck, oldValue, oldValue + 1);
						WordOnDeck = "";
						oldValue = 0;
					} else {
						counter.put(WordOnDeck, 1);
						WordOnDeck = "";
					}
				}
			}


		System.out.println(counter);




		return counter;
	}

	/**
	 * 7. Implement a binary search algorithm.
	 * 
	 * Searching a sorted collection is a common task. A dictionary is a sorted list
	 * of word definitions. Given a word, one can find its definition. A telephone
	 * book is a sorted list of people's names, addresses, and telephone numbers.
	 * Knowing someone's name allows one to quickly find their telephone number and
	 * address.
	 * 
	 * If the list to be searched contains more than a few items (a dozen, say) a
	 * binary search will require far fewer comparisons than a linear search, but it
	 * imposes the requirement that the list be sorted.
	 * 
	 * In computer science, a binary search or half-interval search algorithm finds
	 * the position of a specified input value (the search "key") within an array
	 * sorted by key value.
	 * 
	 * In each step, the algorithm compares the search key value with the key value
	 * of the middle element of the array.
	 * 
	 * If the keys match, then a matching element has been found and its index, or
	 * position, is returned.
	 * 
	 * Otherwise, if the search key is less than the middle element's key, then the
	 * algorithm repeats its action on the sub-array to the left of the middle
	 * element or, if the search key is greater, on the sub-array to the right.
	 * 
	 * If the remaining array to be searched is empty, then the key cannot be found
	 * in the array and a special "not found" indication is returned.
	 * 
	 * A binary search halves the number of items to check with each iteration, so
	 * locating an item (or determining its absence) takes logarithmic time. A
	 * binary search is a dichotomic divide and conquer search algorithm.
	 * 
	 */
	static class BinarySearch<T> {
		private List<T> sortedList;

		public int indexOf(T t) {
			// TODO Write an implementation for this method declaration
			return 0;
		}

		public BinarySearch(List<T> sortedList) {
			super();
			this.sortedList = sortedList;
		}

		public List<T> getSortedList() {
			return sortedList;
		}

		public void setSortedList(List<T> sortedList) {
			this.sortedList = sortedList;
		}

	}

	/**
	 * 8. Create an implementation of the rotational cipher, also sometimes called
	 * the Caesar cipher.
	 *
	 * The Caesar cipher is a simple shift cipher that relies on transposing all the
	 * letters in the alphabet using an integer key between 0 and 26. Using a key of
	 * 0 or 26 will always yield the same output due to modular arithmetic. The
	 * letter is shifted for as many values as the value of the key.
	 *
	 * The general notation for rotational ciphers is ROT + <key>. The most commonly
	 * used rotational cipher is ROT13.
	 *
	 * A ROT13 on the Latin alphabet would be as follows:
	 *
	 * Plain: abcdefghijklmnopqrstuvwxyz Cipher: nopqrstuvwxyzabcdefghijklm It is
	 * stronger than the Atbash cipher because it has 27 possible keys, and 25
	 * usable keys.
	 *
	 * Ciphertext is written out in the same formatting as the input including
	 * spaces and punctuation.
	 *
	 * Examples: ROT5 omg gives trl ROT0 c gives c ROT26 Cool gives Cool ROT13 The
	 * quick brown fox jumps over the lazy dog. gives Gur dhvpx oebja sbk whzcf bire
	 * gur ynml qbt. ROT13 Gur dhvpx oebja sbk whzcf bire gur ynml qbt. gives The
	 * quick brown fox jumps over the lazy dog.
	 */
	static class RotationalCipher {
		private int key;

		public RotationalCipher(int key) {
			super();
			this.key = key;
		}

		public String rotate(String plaintext) {
			String cipher = "";

			HashMap<Character,Integer> codeUpper = new HashMap<>();
			HashMap<Character,Integer> codeLower = new HashMap<>();
			HashMap<Integer,Character> codeUpperGet = new HashMap<>();
			HashMap<Integer,Character> codeLowerGet = new HashMap<>();

			codeUpper.put('A',1);
			codeUpper.put('B',2);
			codeUpper.put('C',3);
			codeUpper.put('D',4);
			codeUpper.put('E',5);
			codeUpper.put('F',6);
			codeUpper.put('G',7);
			codeUpper.put('H',8);
			codeUpper.put('I',9);
			codeUpper.put('J',10);
			codeUpper.put('K',11);
			codeUpper.put('L',12);
			codeUpper.put('M',13);
			codeUpper.put('N',14);
			codeUpper.put('O',15);
			codeUpper.put('P',16);
			codeUpper.put('Q',17);
			codeUpper.put('R',18);
			codeUpper.put('S',19);
			codeUpper.put('T',20);
			codeUpper.put('U',21);
			codeUpper.put('V',22);
			codeUpper.put('W',23);
			codeUpper.put('X',24);
			codeUpper.put('Y',25);
			codeUpper.put('Z',26);

			codeUpperGet.put(1,'A');
			codeUpperGet.put(2,'B');
			codeUpperGet.put(3,'C');
			codeUpperGet.put(4,'D');
			codeUpperGet.put(5,'E');
			codeUpperGet.put(6,'F');
			codeUpperGet.put(7,'G');
			codeUpperGet.put(8,'H');
			codeUpperGet.put(9,'I');
			codeUpperGet.put(10,'J');
			codeUpperGet.put(11,'K');
			codeUpperGet.put(12,'L');
			codeUpperGet.put(13,'M');
			codeUpperGet.put(14,'N');
			codeUpperGet.put(15,'O');
			codeUpperGet.put(16,'P');
			codeUpperGet.put(17,'Q');
			codeUpperGet.put(18,'R');
			codeUpperGet.put(19,'S');
			codeUpperGet.put(20,'T');
			codeUpperGet.put(21,'U');
			codeUpperGet.put(22,'V');
			codeUpperGet.put(23,'W');
			codeUpperGet.put(24,'X');
			codeUpperGet.put(25,'Y');
			codeUpperGet.put(26,'Z');
			codeUpperGet.put(27,'A');
			codeUpperGet.put(28,'B');
			codeUpperGet.put(29,'C');
			codeUpperGet.put(30,'D');
			codeUpperGet.put(31,'E');
			codeUpperGet.put(32,'F');
			codeUpperGet.put(33,'G');
			codeUpperGet.put(34,'H');
			codeUpperGet.put(35,'I');
			codeUpperGet.put(36,'J');
			codeUpperGet.put(37,'K');
			codeUpperGet.put(38,'L');
			codeUpperGet.put(39,'M');
			codeUpperGet.put(40,'N');
			codeUpperGet.put(41,'O');
			codeUpperGet.put(42,'P');
			codeUpperGet.put(43,'Q');
			codeUpperGet.put(44,'R');
			codeUpperGet.put(45,'S');
			codeUpperGet.put(46,'T');
			codeUpperGet.put(47,'U');
			codeUpperGet.put(48,'V');
			codeUpperGet.put(49,'W');
			codeUpperGet.put(50,'X');
			codeUpperGet.put(51,'Y');
			codeUpperGet.put(52,'Z');

			codeLower.put('a',1);
			codeLower.put('b',2);
			codeLower.put('c',3);
			codeLower.put('d',4);
			codeLower.put('e',5);
			codeLower.put('f',6);
			codeLower.put('g',7);
			codeLower.put('h',8);
			codeLower.put('i',9);
			codeLower.put('j',10);
			codeLower.put('k',11);
			codeLower.put('l',12);
			codeLower.put('m',13);
			codeLower.put('n',14);
			codeLower.put('o',15);
			codeLower.put('p',16);
			codeLower.put('q',17);
			codeLower.put('r',18);
			codeLower.put('s',19);
			codeLower.put('t',20);
			codeLower.put('u',21);
			codeLower.put('v',22);
			codeLower.put('w',23);
			codeLower.put('x',24);
			codeLower.put('y',25);
			codeLower.put('z',26);

			codeLowerGet.put(1,'a');
			codeLowerGet.put(2,'b');
			codeLowerGet.put(3,'c');
			codeLowerGet.put(4, 'd');
			codeLowerGet.put(5, 'e');
			codeLowerGet.put(6,'f');
			codeLowerGet.put(7,'g');
			codeLowerGet.put(8,'h');
			codeLowerGet.put(9,'i');
			codeLowerGet.put(10,'j');
			codeLowerGet.put(11,'k');
			codeLowerGet.put(12,'l');
			codeLowerGet.put(13,'m');
			codeLowerGet.put(14,'n');
			codeLowerGet.put(15,'o');
			codeLowerGet.put(16,'p');
			codeLowerGet.put(17,'q');
			codeLowerGet.put(18,'r');
			codeLowerGet.put(19,'s');
			codeLowerGet.put(20,'t');
			codeLowerGet.put(21,'u');
			codeLowerGet.put(22,'v');
			codeLowerGet.put(23,'w');
			codeLowerGet.put(24,'x');
			codeLowerGet.put(25,'y');
			codeLowerGet.put(26,'z');
			codeLowerGet.put(27,'a');
			codeLowerGet.put(28,'b');
			codeLowerGet.put(29,'c');
			codeLowerGet.put(30, 'd');
			codeLowerGet.put(31, 'e');
			codeLowerGet.put(32,'f');
			codeLowerGet.put(33,'g');
			codeLowerGet.put(34,'h');
			codeLowerGet.put(35,'i');
			codeLowerGet.put(36,'j');
			codeLowerGet.put(37,'k');
			codeLowerGet.put(38,'l');
			codeLowerGet.put(39,'m');
			codeLowerGet.put(40,'n');
			codeLowerGet.put(41,'o');
			codeLowerGet.put(42,'p');
			codeLowerGet.put(43,'q');
			codeLowerGet.put(44,'r');
			codeLowerGet.put(45,'s');
			codeLowerGet.put(46,'t');
			codeLowerGet.put(47,'u');
			codeLowerGet.put(48,'v');
			codeLowerGet.put(49,'w');
			codeLowerGet.put(50,'x');
			codeLowerGet.put(51,'y');
			codeLowerGet.put(52,'z');

			for( int i =0; i < plaintext.length(); i++)

				if (plaintext.charAt(i) == ' ') {cipher = cipher + ' ';}
				else if (plaintext.charAt(i) == 'a' || plaintext.charAt(i) == 'b' || plaintext.charAt(i) == 'c' ||
				plaintext.charAt(i) == 'd' || plaintext.charAt(i) == 'e' || plaintext.charAt(i) == 'f' ||
				plaintext.charAt(i) == 'g' || plaintext.charAt(i) == 'h' || plaintext.charAt(i) == 'i' ||
				plaintext.charAt(i) == 'j' || plaintext.charAt(i) == 'k' || plaintext.charAt(i) == 'l' ||
				plaintext.charAt(i) == 'm' || plaintext.charAt(i) == 'n' || plaintext.charAt(i) == 'o' ||
				plaintext.charAt(i) == 'p' || plaintext.charAt(i) == 'q' || plaintext.charAt(i) == 'r' ||
				plaintext.charAt(i) == 's' || plaintext.charAt(i) == 't' || plaintext.charAt(i) == 'u' ||
				plaintext.charAt(i) == 'v' || plaintext.charAt(i) == 'w' || plaintext.charAt(i) == 'x' ||
				plaintext.charAt(i) == 'y' || plaintext.charAt(i) == 'z')
						{int value; value = codeLower.get(plaintext.charAt(i));
							cipher = cipher + codeLowerGet.get(value+key);
							 		}

				else if (plaintext.charAt(i) == 'A' || plaintext.charAt(i) == 'B' || plaintext.charAt(i) == 'C' ||
						plaintext.charAt(i) == 'D' || plaintext.charAt(i) == 'E' || plaintext.charAt(i) == 'F' ||
						plaintext.charAt(i) == 'G' || plaintext.charAt(i) == 'H' || plaintext.charAt(i) == 'I' ||
						plaintext.charAt(i) == 'J' || plaintext.charAt(i) == 'K' || plaintext.charAt(i) == 'L' ||
						plaintext.charAt(i) == 'M' || plaintext.charAt(i) == 'N' || plaintext.charAt(i) == 'O' ||
						plaintext.charAt(i) == 'P' || plaintext.charAt(i) == 'Q' || plaintext.charAt(i) == 'R' ||
						plaintext.charAt(i) == 'S' || plaintext.charAt(i) == 'T' || plaintext.charAt(i) == 'U' ||
						plaintext.charAt(i) == 'V' || plaintext.charAt(i) == 'W' || plaintext.charAt(i) == 'X' ||
						plaintext.charAt(i) == 'Y' || plaintext.charAt(i) == 'Z')
				{int value; value = codeUpper.get(plaintext.charAt(i));

					cipher = cipher + codeUpperGet.get(value+key);
					}

				else { cipher = cipher + plaintext.charAt(i); }


			return cipher;
		}

	}

	/**
	 * 9. Given a number n, determine what the nth prime is.
	 *
	 * By listing the first six prime numbers: 2, 3, 5, 7, 11, and 13, we can see
	 * that the 6th prime is 13.
	 *
	 * If your language provides methods in the standard library to deal with prime
	 * numbers, pretend they don't exist and implement them yourself.
	 *
	 * @param n
	 * @return
	 */
	public int calculateNthPrime(int n) {

		return 0;
	}

	/**
	 * 10. Determine if a sentence is a pangram. A pangram (Greek: παν γράμμα, pan
	 * gramma, "every letter") is a sentence using every letter of the alphabet at
	 * least once. The best known English pangram is:
	 *
	 * The quick brown fox jumps over the lazy dog.
	 *
	 * The alphabet used consists of ASCII letters a to z, inclusive, and is case
	 * insensitive. Input will not contain non-ASCII symbols.
	 *
	 * @param s
	 * @return
	 */
	public boolean isPangram(String words) {
		words = words.toLowerCase();
		boolean verdict = false;
		boolean a = false;
		boolean b = false;
		boolean c = false;
		boolean d = false;
		boolean e = false;
		boolean f = false;
		boolean g = false;
		boolean h = false;
		boolean i = false;
		boolean j = false;
		boolean k = false;
		boolean l = false;
		boolean m = false;
		boolean n = false;
		boolean o = false;
		boolean p = false;
		boolean q = false;
		boolean r = false;
		boolean s = false;
		boolean t = false;
		boolean u = false;
		boolean v = false;
		boolean w = false;
		boolean x = false;
		boolean y = false;
		boolean z = false;

		for (int iterator = 0; iterator < words.length(); iterator++  )
		{if 	(words.charAt(iterator) == 'a') {a = true;}
		else if	(words.charAt(iterator) == 'b') {b = true;}
		else if	(words.charAt(iterator) == 'c') {c = true;}
		else if	(words.charAt(iterator) == 'd') {d = true;}
		else if	(words.charAt(iterator) == 'e') {e = true;}
		else if	(words.charAt(iterator) == 'f') {f = true;}
		else if	(words.charAt(iterator) == 'g') {g = true;}
		else if	(words.charAt(iterator) == 'h') {h = true;}
		else if	(words.charAt(iterator) == 'i') {i = true;}
		else if	(words.charAt(iterator) == 'j') {j = true;}
		else if	(words.charAt(iterator) == 'k') {k = true;}
		else if	(words.charAt(iterator) == 'l') {l = true;}
		else if	(words.charAt(iterator) == 'm') {m = true;}
		else if	(words.charAt(iterator) == 'n') {n = true;}
		else if	(words.charAt(iterator) == 'o') {o = true;}
		else if	(words.charAt(iterator) == 'p') {p = true;}
		else if	(words.charAt(iterator) == 'q') {q = true;}
		else if	(words.charAt(iterator) == 'r') {r = true;}
		else if	(words.charAt(iterator) == 's') {s = true;}
		else if	(words.charAt(iterator) == 't') {t = true;}
		else if	(words.charAt(iterator) == 'u') {u = true;}
		else if	(words.charAt(iterator) == 'v') {v = true;}
		else if	(words.charAt(iterator) == 'w') {w = true;}
		else if	(words.charAt(iterator) == 'x') {x = true;}
		else if	(words.charAt(iterator) == 'y') {y = true;}
		else if	(words.charAt(iterator) == 'z') {z = true;}	}

		if (a && b && c && d && e && f && g && h && i && j && k && l && m && n && o &&
				p && q && r && s && t && u && v && w && x && y && z)
		{verdict = true;}
		else {verdict = false;}


		return verdict;




	}

	/**
	 * 11. Implement a program that translates from English to Pig Latin.
	 * 
	 * Pig Latin is a made-up children's language that's intended to be confusing.
	 * It obeys a few simple rules (below), but when it's spoken quickly it's really
	 * difficult for non-children (and non-native speakers) to understand.
	 * 
	 * Rule 1: If a word begins with a vowel sound, add an "ay" sound to the end of
	 * the word. Rule 2: If a word begins with a consonant sound, move it to the end
	 * of the word, and then add an "ay" sound to the end of the word. There are a
	 * few more rules for edge cases, and there are regional variants too.
	 * 
	 * See http://en.wikipedia.org/wiki/Pig_latin for more details.
	 * 
	 * @param string
	 * @return
	 */
	public String toPigLatin(String string) {
		// TODO Write an implementation for this method declaration
		return null;
	}

	/**
	 * 12. An Armstrong number is a number that is the sum of its own digits each
	 * raised to the power of the number of digits.
	 * 
	 * For example:
	 * 
	 * 9 is an Armstrong number, because 9 = 9^1 = 9 10 is not an Armstrong number,
	 * because 10 != 1^2 + 0^2 = 2 153 is an Armstrong number, because: 153 = 1^3 +
	 * 5^3 + 3^3 = 1 + 125 + 27 = 153 154 is not an Armstrong number, because: 154
	 * != 1^3 + 5^3 + 4^3 = 1 + 125 + 64 = 190 Write some code to determine whether
	 * a number is an Armstrong number.
	 * 
	 * @param input
	 * @return
	 */
	public boolean isArmstrongNumber(int input) {
		// TODO Write an implementation for this method declaration
		return false;
	}

	/**
	 * 13. Compute the prime factors of a given natural number.
	 * 
	 * A prime number is only evenly divisible by itself and 1.
	 * 
	 * Note that 1 is not a prime number.
	 * 
	 * @param l
	 * @return
	 */
	public List<Long> calculatePrimeFactorsOf(long l) {
		// TODO Write an implementation for this method declaration
		return null;
	}



	/**
	 * 14 & 15. Create an implementation of the atbash cipher, an ancient encryption
	 * system created in the Middle East.
	 * 
	 * The Atbash cipher is a simple substitution cipher that relies on transposing
	 * all the letters in the alphabet such that the resulting alphabet is
	 * backwards. The first letter is replaced with the last letter, the second with
	 * the second-last, and so on.
	 * 
	 * An Atbash cipher for the Latin alphabet would be as follows:
	 * 
	 * Plain: abcdefghijklmnopqrstuvwxyz Cipher: zyxwvutsrqponmlkjihgfedcba It is a
	 * very weak cipher because it only has one possible key, and it is a simple
	 * monoalphabetic substitution cipher. However, this may not have been an issue
	 * in the cipher's time.
	 * 
	 * Ciphertext is written out in groups of fixed length, the traditional group
	 * size being 5 letters, and punctuation is excluded. This is to make it harder
	 * to guess things based on word boundaries.
	 * 
	 * Examples Encoding test gives gvhg Decoding gvhg gives test Decoding gsvjf
	 * rxpyi ldmul cqfnk hlevi gsvoz abwlt gives thequickbrownfoxjumpsoverthelazydog
	 *
	 */
	static class AtbashCipher {

		/**
		 * Question 14
		 * 
		 * @param string
		 * @return
		 */
		public static String encode(String string) {
			// TODO Write an implementation for this method declaration
			return null;
		}

		/**
		 * Question 15
		 * 
		 * @param string
		 * @return
		 */
		public static String decode(String string) {
			// TODO Write an implementation for this method declaration
			return null;
		}
	}

	/**
	 * 16. The ISBN-10 verification process is used to validate book identification
	 * numbers. These normally contain dashes and look like: 3-598-21508-8
	 * 
	 * ISBN The ISBN-10 format is 9 digits (0 to 9) plus one check character (either
	 * a digit or an X only). In the case the check character is an X, this
	 * represents the value '10'. These may be communicated with or without hyphens,
	 * and can be checked for their validity by the following formula:
	 * 
	 * (x1 * 10 + x2 * 9 + x3 * 8 + x4 * 7 + x5 * 6 + x6 * 5 + x7 * 4 + x8 * 3 + x9
	 * * 2 + x10 * 1) mod 11 == 0 If the result is 0, then it is a valid ISBN-10,
	 * otherwise it is invalid.
	 * 
	 * Example Let's take the ISBN-10 3-598-21508-8. We plug it in to the formula,
	 * and get:
	 * 
	 * (3 * 10 + 5 * 9 + 9 * 8 + 8 * 7 + 2 * 6 + 1 * 5 + 5 * 4 + 0 * 3 + 8 * 2 + 8 *
	 * 1) mod 11 == 0 Since the result is 0, this proves that our ISBN is valid.
	 * 
	 * @param string
	 * @return
	 */
	public boolean isValidIsbn(String string) {
		// TODO Write an implementation for this method declaration
		return false;
	}


	/**
	 * 17. Calculate the moment when someone has lived for 10^9 seconds.
	 * 
	 * A gigasecond is 109 (1,000,000,000) seconds.
	 * 
	 * @param given
	 * @return
	 */
	public Temporal getGigasecondDate(Temporal given) {
		// TODO Write an implementation for this method declaration
		return null;
	}

	/**
	 * 18. Given a number, find the sum of all the unique multiples of particular
	 * numbers up to but not including that number.
	 * 
	 * If we list all the natural numbers below 20 that are multiples of 3 or 5, we
	 * get 3, 5, 6, 9, 10, 12, 15, and 18.
	 * 
	 * The sum of these multiples is 78.
	 * 
	 * @param i
	 * @param set
	 * @return
	 */
	public int getSumOfMultiples(int i, int[] set) {
		// TODO Write an implementation for this method declaration
		return 0;
	}

	/**
	 * 19. Given a number determine whether or not it is valid per the Luhn formula.
	 * 
	 * The Luhn algorithm is a simple checksum formula used to validate a variety of
	 * identification numbers, such as credit card numbers and Canadian Social
	 * Insurance Numbers.
	 * 
	 * The task is to check if a given string is valid.
	 * 
	 * Validating a Number Strings of length 1 or less are not valid. Spaces are
	 * allowed in the input, but they should be stripped before checking. All other
	 * non-digit characters are disallowed.
	 * 
	 * Example 1: valid credit card number 1 4539 1488 0343 6467 The first step of
	 * the Luhn algorithm is to double every second digit, starting from the right.
	 * We will be doubling
	 * 
	 * 4_3_ 1_8_ 0_4_ 6_6_ If doubling the number results in a number greater than 9
	 * then subtract 9 from the product. The results of our doubling:
	 * 
	 * 8569 2478 0383 3437 Then sum all of the digits:
	 * 
	 * 8+5+6+9+2+4+7+8+0+3+8+3+3+4+3+7 = 80 If the sum is evenly divisible by 10,
	 * then the number is valid. This number is valid!
	 * 
	 * Example 2: invalid credit card number 1 8273 1232 7352 0569 Double the second
	 * digits, starting from the right
	 * 
	 * 7253 2262 5312 0539 Sum the digits
	 * 
	 * 7+2+5+3+2+2+6+2+5+3+1+2+0+5+3+9 = 57 57 is not evenly divisible by 10, so
	 * this number is not valid.
	 * 
	 * @param string
	 * @return
	 */
	public boolean isLuhnValid(String string) {
		// TODO Write an implementation for this method declaration
		return false;
	}

	/**
	 * 20. Parse and evaluate simple math word problems returning the answer as an
	 * integer.
	 * 
	 * Add two numbers together.
	 * 
	 * What is 5 plus 13?
	 * 
	 * 18
	 * 
	 * Now, perform the other three operations.
	 * 
	 * What is 7 minus 5?
	 * 
	 * 2
	 * 
	 * What is 6 multiplied by 4?
	 * 
	 * 24
	 * 
	 * What is 25 divided by 5?
	 * 
	 * 5
	 * 
	 * @param string
	 * @return
	 */
	public int solveWordProblem(String string) {
		// TODO Write an implementation for this method declaration
		return 0;
	}

}
